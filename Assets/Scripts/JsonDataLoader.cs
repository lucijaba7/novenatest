using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LoadJsonData", order = 1)]
public class JsonDataLoader : ScriptableObject
{
    [HideInInspector]
    public TranslatedContent translatedContent;

    public void LoadJsonData()
    {
        string filePath = Path.Combine(Application.persistentDataPath, "data.json");
        if (File.Exists(filePath))
        {
            string jsonContent = File.ReadAllText(filePath);
            translatedContent = JsonUtility.FromJson<TranslatedContent>(jsonContent);
        }
        else
        {
            Debug.LogError("JSON file not found at: " + filePath);
        }
    }

    public List<Topic> getTopics(int languageId)
    {
        return translatedContent.TranslatedContents.Find(lang => lang.LanguageId == languageId).Topics;
    }

    public List<Media> getMedia(int languageId, string topicName)
    {
        return getTopics(languageId).Find(topic => topic.Name == topicName).Media;
    }
}
