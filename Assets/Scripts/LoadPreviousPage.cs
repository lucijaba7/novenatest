using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadPreviousPage : MonoBehaviour
{
    private Button button;
    void Start()
    {
        button = gameObject.GetComponent<Button>();
        if (button != null)
        {
            button.onClick.AddListener(OnBackButtonPressed);
        }
    }

    private void OnBackButtonPressed()
    {
        SceneHistoryManager.GoBack();
    }
}
