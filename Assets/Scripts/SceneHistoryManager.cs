using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;


public static class SceneHistoryManager
{
    private static Stack<string> historyStack = new Stack<string>();

    public static void LoadScene(string sceneName)
    {
        historyStack.Push(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(sceneName);
    }

    public static void GoBack()
    {
        if (historyStack.Count > 0)
        {
            string sceneName = historyStack.Pop();
            SceneManager.LoadScene(sceneName);
        }
        else
        {
            Debug.Log("No previous scene in history.");
        }
    }
}
