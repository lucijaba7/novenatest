using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossManager : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    private Animator bossAnimator;

    // for attack animation
    // private float time = 0.0f;
    // void Start()
    // {
    //     bossAnimator = gameObject.GetComponent<Animator>();
    //     InvokeRepeating("AttackPlayer", 5.0f, 15.0f);
    //     agent.speed = 0.005f;
    // }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(player.position);
        transform.LookAt(player);
    }

    // private void AttackPlayer()
    // {
    //     agent.SetDestination(transform.position);
    //     // bossAnimator.SetTrigger("attack");
    //     transform.LookAt(player);
    //     StartCoroutine(ShootAtPlayer());

    // }
    // IEnumerator ShootAtPlayer()
    // {
    //     yield return new WaitForSeconds(1);
    //     float fireRate = 0.1f;
    //     do
    //     {
    //         // shootingSounds[0].Play(0);
    //         // Rigidbody rb = Instantiate(bullet, bulletPosition.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
    //         // rb.transform.LookAt(player);
    //         // rb.transform.Rotate(new Vector3(0, 90, 0));
    //         // Vector3 temp = (player.position - transform.position);
    //         // Vector3 positionToShoot = new Vector3(temp.x, -3f, temp.z);
    //         // rb.AddForce(positionToShoot.normalized * 30f, ForceMode.Impulse);

    //         // time += Time.deltaTime + fireRate;
    //         // yield return new WaitForSeconds(fireRate);
    //     } while (time < 2.5f);

    //     time = 0.0f;

    // }
}
