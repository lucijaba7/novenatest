using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PauseGameManager
{
    private static bool isPaused = false;

    public static bool TogglePause()
    {
        isPaused = !isPaused;
        Time.timeScale = isPaused ? 0 : 1;
        return isPaused;
    }


}
