using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReturnToMainMenu : MonoBehaviour
{
    private Button menuButton;

    void Start()
    {
        menuButton = GetComponent<Button>();
        menuButton.onClick.AddListener(ReloadGame);
    }

    private void ReloadGame()
    {
        SceneHistoryManager.LoadScene("LanguagePage");
    }

}


