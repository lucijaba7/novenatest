using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShootingManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject Player;

    //bullet 
    public GameObject bullet;
    //bullet force
    public float shootForce;
    public Transform attackPoint;
    public Camera Cam;

    private Animator PlayerAnimator;
    void Start()
    {
        PlayerAnimator = Player.GetComponent<Animator>();
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        PlayerAnimator.SetBool("Shooting", true);
        InvokeRepeating("Shoot", 0f, 0.2f); // Start repeating
    }

    public void OnPointerUp(PointerEventData eventData)
    {

        PlayerAnimator.SetBool("Shooting", false);
        CancelInvoke("Shoot"); // Stop repeating
    }

    void Shoot()
    {
        //Find the exact hit position using a raycast
        Ray ray = Cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Just a ray through the middle of your current view

        //Get target point
        Vector3 targetPoint;
        targetPoint = ray.GetPoint(75); //Just a point far away from the player
        targetPoint.y += 3.0f;

        // Rotate the player to look at the target point
        Vector3 lookPos = targetPoint;
        lookPos.y = 3.0f; // Ignore Y-axis difference
        Player.transform.LookAt(lookPos);

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionToTarget = targetPoint - attackPoint.position;

        //Instantiate bullet/projectile
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity); //store instantiated bullet in currentBullet


        //Add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionToTarget.normalized * shootForce, ForceMode.Impulse);
    }
}
