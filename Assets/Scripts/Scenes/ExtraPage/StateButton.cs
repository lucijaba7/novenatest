using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateButton : MonoBehaviour
{
    public GameObject canvas;
    private Button pauseButton;

    void Start()
    {
        pauseButton = GetComponent<Button>();
        pauseButton.onClick.AddListener(toggleState);
    }

    private void toggleState()
    {
        bool isPaused = PauseGameManager.TogglePause();
        canvas.SetActive(isPaused);
    }
}


