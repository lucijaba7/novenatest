using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LanguageButtonManager : MonoBehaviour
{
    public GameObject buttonPrefab;
    Transform buttonContainer;
    public JsonDataLoader jsonDataLoader;


    private void Start()
    {
        jsonDataLoader.LoadJsonData();
        buttonContainer = gameObject.transform;
        CreateLanguageButtons();
    }

    private void CreateLanguageButtons()
    {

        foreach (LanguageContent language in jsonDataLoader.translatedContent.TranslatedContents)
        {
            GameObject languageButton = Instantiate(buttonPrefab, buttonContainer);

            TextMeshProUGUI buttonText = languageButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = language.LanguageName;

            Button button = languageButton.GetComponent<Button>();
            button.onClick.AddListener(() => OnLanguageButtonClicked(language.LanguageId));
        }

    }

    private void OnLanguageButtonClicked(int languageId)
    {
        PlayerPrefs.SetInt("LanguageId", languageId);
        SceneHistoryManager.LoadScene("TopicListPage");
    }
}

