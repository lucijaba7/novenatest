using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;

public class DataHandler : MonoBehaviour
{
    // private string jsonFileURL = "https://raw.githubusercontent.com/lucijaba7/TestData/main/data.json";
    // private string fileListURL = "https://raw.githubusercontent.com/lucijaba7/TestData/main/fileList.txt";
    // private string mediaFolderURL = "https://github.com/lucijaba7/TestData/blob/main/files";
    private string jsonFileURL = Path.Combine(Application.streamingAssetsPath, "data.json");
    private string fileListURL = Path.Combine(Application.streamingAssetsPath, "fileList.txt");
    private string mediaFolderURL = Path.Combine(Application.streamingAssetsPath, "files");

    private string destinationFolderPath;
    private List<string> fileNames = new List<string>();

    public Slider loadingSlider;



    private void Start()
    {
        destinationFolderPath = Application.persistentDataPath;


        loadingSlider.minValue = 0;
        loadingSlider.maxValue = 100;
        loadingSlider.value = 0;

        StartCoroutine(DownloadData());

    }

    private IEnumerator DownloadData()
    {
        Directory.CreateDirectory(destinationFolderPath);
        Directory.CreateDirectory(destinationFolderPath + "/files");

        yield return StartCoroutine(DownloadJson(jsonFileURL, Path.Combine(destinationFolderPath, "data.json")));

        yield return StartCoroutine(GetFileList());

        foreach (string fileName in fileNames)
        {
            string mediaFileURL = Path.Combine(mediaFolderURL, fileName);
            string destinationFilePath = Path.Combine(destinationFolderPath + "/files", fileName);

            string fileExtension = GetFileExtension(fileName);

            if (fileExtension == "jpg")
            {
                yield return StartCoroutine(DownloadImage(mediaFileURL, destinationFilePath));
            }
            else if (fileExtension == "wav")
            {
                yield return StartCoroutine(DownloadAudio(mediaFileURL, destinationFilePath));
            }
            else
            {
                Debug.LogWarning("Unsupported file type: " + fileName);
            }
        }

        SceneHistoryManager.LoadScene("LanguagePage");

    }

    IEnumerator GetFileList()
    {
        using (UnityWebRequest www = UnityWebRequest.Get(fileListURL))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Failed to retrieve file list: " + www.error);
            }
            else
            {
                string fileListText = www.downloadHandler.text;
                string[] lines = fileListText.Split('\n');

                foreach (string line in lines)
                {
                    string trimmedLine = line.Trim();
                    if (!string.IsNullOrEmpty(trimmedLine))
                    {
                        fileNames.Add(trimmedLine);
                    }
                }

                Debug.Log("File list retrieved successfully:");
                loadingSlider.maxValue = fileNames.Count;
            }
        }
    }


    IEnumerator DownloadJson(string url, string filePath)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Download failed: " + www.error);
            }
            else
            {
                File.WriteAllBytes(filePath, www.downloadHandler.data);
                Debug.Log("Downloaded: " + filePath);
            }
        }
    }


    IEnumerator DownloadImage(string url, string filePath)
    {
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Image download failed: " + www.error);
            }
            else
            {
                // DownloadHandlerTexture downloadHandlerTexture = www.downloadHandler as DownloadHandlerTexture;
                Texture2D downloadedImage = DownloadHandlerTexture.GetContent(www);

                // Convert the Texture2D to a byte array
                byte[] imageData = downloadedImage.EncodeToJPG();


                File.WriteAllBytes(filePath, www.downloadHandler.data);

                Debug.Log("Downloaded: " + filePath);
                loadingSlider.value += 1;
            }
        }
    }


    IEnumerator DownloadAudio(string url, string filePath)
    {
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Audio download failed: " + www.error);
            }
            else
            {

                AudioClip downloadedAudio = DownloadHandlerAudioClip.GetContent(www);
                WavUtility.SaveWavFile(downloadedAudio, filePath);
                loadingSlider.value += 1;

            }
        }
    }

    private string GetFileExtension(string fileName)
    {
        int dotIndex = fileName.LastIndexOf('.');
        if (dotIndex >= 0 && dotIndex < fileName.Length - 1)
        {
            return fileName.Substring(dotIndex + 1).ToLower();
        }
        return string.Empty;
    }


}
