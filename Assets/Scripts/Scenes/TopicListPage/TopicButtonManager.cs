using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TopicData
{
    public int TopicIndex;
    public string TopicName;

    public TopicData(int i, string name)
    {
        TopicIndex = i;
        TopicName = name;
    }
}

public class TopicButtonManager : MonoBehaviour
{
    public GameObject buttonPrefab;
    private Transform buttonContainer;
    public JsonDataLoader jsonDataLoader;


    private void Start()
    {
        buttonContainer = gameObject.transform;
        CreateTopicButtons();
    }


    private void CreateTopicButtons()
    {
        int currentLanguage = PlayerPrefs.GetInt("LanguageId");
        int i = 1;

        foreach (Topic topic in jsonDataLoader.getTopics(currentLanguage))
        {
            GameObject topicButton = Instantiate(buttonPrefab, buttonContainer);

            // Access the button's text component and set the topic name.
            TextMeshProUGUI buttonText = topicButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = topic.Name;

            // Set butotn index
            Transform buttonCircle = topicButton.transform.GetChild(1);
            TextMeshProUGUI buttonIndex = buttonCircle.transform.GetComponentInChildren<TextMeshProUGUI>();
            buttonIndex.text = i.ToString();

            Button button = topicButton.GetComponent<Button>();
            TopicData currentData = new TopicData(i, topic.Name);
            button.onClick.AddListener(() => OnTopicButtonClicked(currentData));

            i++;
        }

    }

    private void OnTopicButtonClicked(TopicData data)
    {
        PlayerPrefs.SetString("TopicName", data.TopicName);
        PlayerPrefs.SetInt("TopicIndex", data.TopicIndex);

        SceneHistoryManager.LoadScene("TopicPage");
    }
}

