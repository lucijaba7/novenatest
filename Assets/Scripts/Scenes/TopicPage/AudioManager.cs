using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.IO;
using TMPro;


public class AudioManager : MonoBehaviour
{

    public Image buttonImage;
    public Sprite playImage;
    public Sprite pauseImage;

    public TextMeshProUGUI timerText;

    private bool isPlaying = false;
    private AudioSource audioSource;
    private Button playPauseButton;

    void Start()
    {
        playPauseButton = GetComponent<Button>();
        playPauseButton.onClick.AddListener(() => TogglePlayPause());
        UpdateButtonImage();
    }

    void Update()
    {
        if (audioSource != null)
        {
            UpdateTimer();
        }
    }

    public void InitializeAudioData(string filePath)
    {
        StartCoroutine(LoadAudio(filePath));
    }

    IEnumerator LoadAudio(string audioFileName)
    {
        string audioPath = Path.Combine(Application.persistentDataPath, audioFileName);

        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip("file://" + audioPath, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Error while loading audio: " + www.error);
            }
            else
            {
                AudioClip clip = DownloadHandlerAudioClip.GetContent(www);
                audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.clip = clip;
            }
        }
    }

    private void UpdateButtonImage()
    {
        buttonImage.sprite = isPlaying ? pauseImage : playImage;

    }

    public void TogglePlayPause()
    {

        if (audioSource != null)
        {
            if (isPlaying)
            {
                audioSource.Pause();
            }
            else
            {

                audioSource.Play();
            }

            isPlaying = !isPlaying;
            UpdateButtonImage();
        }

    }

    private void UpdateTimer()
    {
        string currentTimeString = FormatTime(audioSource.time);
        string totalTimeString = FormatTime(audioSource.clip.length);

        timerText.text = $"{currentTimeString} / {totalTimeString}";
    }

    private string FormatTime(float timeInSeconds)
    {
        int minutes = (int)timeInSeconds / 60;
        int seconds = (int)timeInSeconds % 60;
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }


}
