using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GalleryManager : MonoBehaviour
{
    public float changeInterval = 4f;

    public float driftSpeed = 5f;

    private Image imageComponent;
    private RectTransform rectTransform;
    private List<Photo> photos;
    private int currentImageIndex = 0;


    void Start()
    {
        imageComponent = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
    }


    public void InitializeGalleryData(List<Photo> photoList)
    {
        photos = photoList;
        StartCoroutine(ChangeImageRoutine());
    }

    IEnumerator ChangeImageRoutine()
    {
        while (true)
        {

            string path = photos[currentImageIndex].Path;
            string imagePath = Path.Combine(Application.persistentDataPath, path);

            if (File.Exists(imagePath))
            {
                byte[] fileData = File.ReadAllBytes(imagePath);
                Texture2D texture = new Texture2D(2, 2);
                if (texture.LoadImage(fileData))
                {
                    Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    imageComponent.sprite = sprite;

                    // Scale the image to fit
                    float scaleFactor = Screen.height / sprite.rect.height;
                    rectTransform.sizeDelta = new Vector2(sprite.rect.width * scaleFactor, Screen.height);

                }
                else
                {
                    Debug.LogError("Failed to load texture from image data.");
                }
            }
            else
            {
                Debug.LogError("File does not exist at path: " + imagePath);
            }

            // Apply zoom and drift effect
            StartCoroutine(Drift());

            // Wait for the interval and change to the next image
            yield return new WaitForSeconds(changeInterval);
            currentImageIndex = (currentImageIndex + 1) % photos.Count;
        }
    }

    IEnumerator Drift()
    {
        float elapsedTime = 0;
        // Choose a random direction only on the x-axis (left or right)
        Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), 0, 0).normalized;

        while (elapsedTime < changeInterval)
        {
            // Apply the horizontal drift effect
            rectTransform.localPosition += randomDirection * driftSpeed * Time.deltaTime;

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
