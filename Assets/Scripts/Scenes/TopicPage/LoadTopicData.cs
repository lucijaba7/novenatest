using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadTopicData : MonoBehaviour
{
    private List<Media> media;
    private int topicIndex;
    private string topicName;
    public JsonDataLoader jsonDataLoader;
    public TextMeshProUGUI topicIndexText;
    public TextMeshProUGUI topicNameText;
    public GalleryManager galleryManager;
    public AudioManager audioManager;

    void Start()
    {
        media = LoadData();
        ShowData();
        SendMedia();
    }

    private List<Media> LoadData()
    {
        int currentLanguage = PlayerPrefs.GetInt("LanguageId");
        topicName = PlayerPrefs.GetString("TopicName");
        topicIndex = PlayerPrefs.GetInt("TopicIndex");

        return jsonDataLoader.getMedia(currentLanguage, topicName);
    }

    private void ShowData()
    {
        topicIndexText.text = topicIndex.ToString();
        topicNameText.text = topicName;
    }

    private void SendMedia()
    {
        foreach (Media m in media)
        {
            if (m.Name == "Gallery")
            {
                if (m.Photos != null)
                {
                    galleryManager.InitializeGalleryData(m.Photos);
                }

            }
            else if (m.Name == "Audio")
            {
                if (m.FilePath != null)
                {
                    audioManager.InitializeAudioData(m.FilePath);
                }

            }
        }
    }

}
