using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Photo
{
    public string Path;
    public string Name;
}

[Serializable]
public class Media
{
    public string Name;
    public List<Photo> Photos;
    public string FilePath;
}

[Serializable]
public class Topic
{
    public string Name;
    [SerializeField]
    public List<Media> Media;
}

[Serializable]
public class LanguageContent
{
    public int LanguageId;
    public string LanguageName;
    public List<Topic> Topics;
}

[Serializable]
public class TranslatedContent
{
    public List<LanguageContent> TranslatedContents;
}

