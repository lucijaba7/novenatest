using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System;

public static class WavUtility
{
    public static bool SaveWavFile(AudioClip clip, string filepath)
    {
        if (clip == null)
        {
            Debug.LogError("No AudioClip found to save as WAV file.");
            return false;
        }

        // Convert the AudioClip data to WAV format
        byte[] wavData = ConvertAudioClipToWav(clip);
        if (wavData == null)
        {
            return false;
        }

        try
        {
            File.WriteAllBytes(filepath, wavData);
            Debug.Log("WAV file saved successfully to: " + filepath);
            return true;
        }
        catch (Exception ex)
        {
            Debug.LogError("Failed to save WAV file: " + ex.Message);
            return false;
        }
    }

    private static byte[] ConvertAudioClipToWav(AudioClip clip)
    {
        var samples = new float[clip.samples * clip.channels];
        clip.GetData(samples, 0);

        int headerSize = 44; // default for WAV
        byte[] wavFile = new byte[samples.Length * 2 + headerSize];
        Buffer.BlockCopy(BitConverter.GetBytes(0x46464952), 0, wavFile, 0, 4); // "RIFF"
        Buffer.BlockCopy(BitConverter.GetBytes(36 + samples.Length * 2), 0, wavFile, 4, 4); // ChunkSize
        Buffer.BlockCopy(BitConverter.GetBytes(0x45564157), 0, wavFile, 8, 4); // "WAVE"
        Buffer.BlockCopy(BitConverter.GetBytes(0x20746D66), 0, wavFile, 12, 4); // "fmt "
        Buffer.BlockCopy(BitConverter.GetBytes(16), 0, wavFile, 16, 4); // Subchunk1Size
        Buffer.BlockCopy(BitConverter.GetBytes((ushort)1), 0, wavFile, 20, 2); // AudioFormat
        Buffer.BlockCopy(BitConverter.GetBytes((ushort)clip.channels), 0, wavFile, 22, 2); // NumChannels
        Buffer.BlockCopy(BitConverter.GetBytes(clip.frequency), 0, wavFile, 24, 4); // SampleRate
        Buffer.BlockCopy(BitConverter.GetBytes(clip.frequency * clip.channels * 2), 0, wavFile, 28, 4); // ByteRate
        Buffer.BlockCopy(BitConverter.GetBytes((ushort)(clip.channels * 2)), 0, wavFile, 32, 2); // BlockAlign
        Buffer.BlockCopy(BitConverter.GetBytes((ushort)16), 0, wavFile, 34, 2); // BitsPerSample
        Buffer.BlockCopy(BitConverter.GetBytes(0x61746164), 0, wavFile, 36, 4); // "data"
        Buffer.BlockCopy(BitConverter.GetBytes(samples.Length * 2), 0, wavFile, 40, 4); // Subchunk2Size

        int offset = 44;
        for (int i = 0; i < samples.Length; i++)
        {
            short val = (short)(samples[i] * 32767);
            Buffer.BlockCopy(BitConverter.GetBytes(val), 0, wavFile, offset, 2);
            offset += 2;
        }

        return wavFile;
    }
}